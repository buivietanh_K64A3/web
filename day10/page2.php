<?php
    require("grade.php")
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel = "stylesheet" href = "style.css">
    <title>Trắc nghiệm</title>
</head>
<body>
    <?php
        $answer6 = isset($_POST['answer5']) ? $_POST['answer5'] : '';
        $answer7 = isset($_POST['answer6']) ? $_POST['answer6'] : '';
        $answer8 = isset($_POST['answer7']) ? $_POST['answer7'] : '';
        $answer9 = isset($_POST['answer8']) ? $_POST['answer8'] : '';
        $answer10 = isset($_POST['answer9']) ? $_POST['answer9'] : '';
        setcookie("answer5", $answer6, time()+3600);
        setcookie("answer6", $answer7, time()+3600);
        setcookie("answer7", $answer8, time()+3600);
        setcookie("answer8", $answer9, time()+3600);
        setcookie("answer9", $answer10, time()+3600);
        if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['submit'])){
            header("Location: page3.php");
        }
    ?>
    <form class = "container" action="" method="post">
        <div class = "box_questions">
            <?php
            for($i=5;$i<count($questions); $i++){
                echo'<div class="question">
                            '.($i+1).'. '.$questions[$i].'
                        </div>';
            
                foreach($results[$i] as $key=>$value){
                    echo'<div class="answers">
                                <input class="radio_answer" type="radio" name="answer'.$i.'" value="'.$key.'"> '.$value.'
                            </div>';
                };
        };  
            ?>
            <div class="btn">
                <button class="noselect" name="submit" type="submit">Nộp bài</button>

            </div>
        </div>
        
    </form>
</body>
</html>