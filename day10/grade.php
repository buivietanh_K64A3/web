<?php
    // Mảng câu hỏi
    $questions = array(
        "PHP tượng trưng cho cái gì:",
        "Đoạn mã nào sau đây được sử dụng để chú thích PHP:",
        "Mặc định của một biến không có giá trị được thể hiện với từ khóa:",
        "Ký hiệu nào được dùng khi sử dụng biến trong PHP:", 
        "Đâu không phải là phép toán được dùng so sánh trong PHP:",
        "Đoạn mã : \<\?php FUNCTION TEST(){ echo 'HELLO WORLD!'; } test();?> sẽ in ra màn hình:",
        "Đáp án nào sau đây không được xác định trước bởi PHP (Magic constants):",
        "Hàm nào sau đây dùng để khai báo hằng số:",
        "Đoạn mã: \$var = 'a';\$VAR = 'b' ; echo \$var\$VAR; sẽ in ra màn hình:",
        "Đoạn mã: class MyException extends Exception {}try { throw new MyException('Oops!');} catch (Exception \$e) { echo Caught Exception;} catch (MyException \$e) { echo Caught MyException;}
        sẽ in ra màn hình:");
        // Mảng đáp án. Đáp án có key =1 là đáp án đúng
    $results = array(
        array( // result of question 1
            1=>"PHP: Hypertext Preprocessor", 
            2=>"Hypertext Transfer Protocol",
            3=>"Preprocessed Hypertext Page",
            4=>"Hypertext Markup Language",
        ),
        array( // result of question 2
            2=>"/* commented code here */", 
            4=>"// you are handsome",
            3=>"# you are beautiful",
            1=>"Tất cả các ý trên",
        ),
        array( // result of question 3
            3=>"none", 
            4=>"undefined",
            1=>"null",
            2=>"Không có đáp án nào đúng",
        ),
        array( // result of question 4
            4=>"$$", 
            3=>"@",
            2=>"#",
            1=>"$",
        ),
        array( // result of question 5
            1=>"<=>", 
            2=>">=",
            3=>"!=",
            4=>"===",
        ),
        array( // result of question 6
            2=>"hello world", 
            1=>"HELLO WORLD!",
            3=>"hello world",
            4=>"Không chạy được, báo lỗi",
        ),
        array( // result of question 7
            3=>" __LINE__", 
            4=>"__FILE__",
            1=>"__DATE__",
            2=>"__METHOD__",
        ),
        array( // result of question 8
            4=>"const", 
            3=>"constant",
            2=>"def",
            1=>"define",
        ),
        array( // result of question 9
            1=>"ab", 
            2=>"Ab",
            3=>"aB",
            4=>"AB",
        ),
        array( // result of question 10
            2=>"Caught MyException", 
            1=>"Caught Exception",
            3=>"Lỗi",
            4=>"Không chạy được",
        ),


);
?>