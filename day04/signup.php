<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php
// define variables and set to empty values
$name = $birthday = $gender = $address =$khoa= "";
$nameErr= $birthdayErr = $genderErr = $addressErr =$khoaErr= "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
      $nameErr = "Hãy nhập tên";
    } else {
      $name = test_input($_POST["name"]);
    }
    
    if (empty($_POST["birthday"])) {
      $birthdayErr = "Hãy nhập ngày sinh";
    } else {
      $birthday = test_input($_POST["birthday"]);

      if((strlen($birthday)<10)OR(strlen($birthday)>10)){
        $birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";
        }
      else{
        
        //The entered value is checked for proper Date format
        if((substr_count($birthday,"/"))<>2){
          $birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";
        }
        else{
          $pos=strpos($birthday,"/");
          $date=substr($birthday,0,($pos));
          
          if(($date<=0)OR($date>31)){$birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";}
          
          $month=substr($birthday,($pos+1),($pos));
          if(($month<=0)OR($month>12)){$birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";}
        }
      }
    }
      
    if (empty($_POST["khoa"])) {
      $khoaErr = "Hãy nhập khoa";
    } else {
      $khoa = test_input($_POST["khoa"]);
    }
  
    if (empty($_POST["gender"])) {
      $genderErr = "Hãy chọn giới tính";
    } else {
      $gender = test_input($_POST["gender"]);
    }
  }

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
    <div class="box">  
        <div class="error">
            <?php
                echo $nameErr ;
                echo "<br>";
                echo $genderErr;
                echo "<br>";
                echo $khoaErr;
                echo "<br>";
                echo $birthdayErr;
                echo $birthday;
                ?>
        </div>    
                <form class="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="row">
                        <div class="inputBox1">
                            <span> Họ và tên <i>*</i></span> 
                            <input class="input1" type="text"  name="name">
                            
                        </div>

                        <div class="inputBox2">
                            <span class="gender">Giới tính<i>*</i></span>
                            <?php
                                $gender=array(0 => "Nam",1 => "Nữ");
                                    for($i = 0; $i < count($gender); $i++){
                                    echo '<input class="input2" type="radio" name="gender" value = '.$gender[$i].'>
                                                <span class="" style= "margin-right:20px;" >'.$gender[$i].'</span>';
                                    }
                            ?>
                            
                        </div>

                        <div class="selectbox">
                            <span class="pk">Phân khoa<i>*</i></span>
                            <select class="input3" name="khoa">
                            <?php
                                $khoa = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học dữ liệu");  
                                foreach ($khoa as $key=>$value) {
                                    echo '<option  value = '.$key.'>'.$value.'</option>';
                                }   
                            ?>
                            </select>
                            
                        </div>

                        <div class="inputBox4">
                            <span> Ngày sinh <i>*</i></span>
                                <input class='input4' id="txtDate" type="text" class="form-control date-input" name="birthday" />
                                <!-- <label class="input-group-btn" for="txtDate">
                                    <span class="btn btn-default">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </label> -->
                           
                        
                        </div>

                        <div class="inputBox5">
                            <span> Địa chỉ </span>
                            <input class="input5" type="text"  name="address">
                            
                        </div>

                        <div class="boxbt" >
                            <button class="bt" type ="submit"> Đăng ký</button>
                        </div>

                        
                    </div>
                </form>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#txtDate').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
                

            });
        });
    </script>
    

</body>
</html>