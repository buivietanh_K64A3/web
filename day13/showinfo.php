<?php
         
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<?php
   
    $_SESSION['khoa'] = isset($_POST['khoa'])?$_POST['khoa']:'';
    $_SESSION['keyword'] = isset($_POST['keyword'])?$_POST['keyword']:'';
    $_SESSION['deletebtn'] = isset($_POST['deletebtn'])?$_POST['deletebtn']:'';

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['addbtn'])) {
        header("Location: signup.php");
        exit();
    }
    if($_SERVER["REQUEST_METHOD"]=='POST' && isset($_POST['editbtn'])){
        header("Location: update.php");
        exit();
    }
?>
    <form class="container" method="post">
        <div class="search">
            <div class="search_khoa">
                <label >Khoa</label>
                <select name="khoa" id="khoa">
                    <?php
                        $khoa=array(""=>"", "MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                        foreach($khoa as $key=>$value){
                            if ($key==$_SESSION['khoa']){
                                echo '<option selected="selected" value='.$key.'>'.$value.'</option>';
                            }
                            else{
                                echo '<option value='.$key.'>'.$value.'</option>';
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="search_keyword_area">
                <label >Từ khóa</label>
                <?php
                    echo '<input type="text" id ="keyword" name="keyword" value="'.trim($_SESSION['keyword']).'">';
                ?>
            </div>
            <div class="search_btn">
                <input  type = 'button' onclick = "myFunction()" name ="delete" value="Xóa"></input>
                <input  type = 'submit' name ="search" value = "Tìm kiếm"></input>
            </div>
        </div>
        
            <?php
                require_once('connection.php');
                $khoa = $_SESSION['khoa'];
                $keyword = $_SESSION['keyword'];
                if($_SERVER["REQUEST_METHOD"]=='POST' && isset($_POST['search'])){
                    if($_SESSION['khoa'] == '' && $_SESSION['keyword'] != ''){
                        $query = "SELECT * FROM student where address LIKE '%$keyword%' or name LIKE '%$keyword%'";
                    }
                    else if($_SESSION['khoa'] != '' && $_SESSION['keyword'] == ''){
                        $query = "SELECT * from student where faculty = '$khoa' ";
                    }
                    else{ $query = "SELECT * FROM student WHERE faculty = '$khoa' and (address LIKE '%$keyword%' OR name LIKE '%$keyword%')";
                    }
                }
                else{
                    $query = "SELECT * from student";
                }
                
                $result = $connection->query($query);
                $data = array();
                while($row = mysqli_fetch_array($result)){
                    $data[] = $row;
                }
                echo '<div class="show">Số sinh viên tìm thấy: '.count($data).'</div>';
            ?>
        <div class="add_btn">
            <input type="submit" value="Thêm" name ="addbtn">
        </div>
        <div class="show_edit_info" style="width = 100%;">
            <table>
                <tr>
                    <th width="7%">No</th>
                    <th width="19%">Tên sinh viên</th>
                    <th width="20%">Khoa</th>
                    <th width="11%">Gender</th>
                    <th width="18%">birthday</th>
                    <th width="50%">address</th>
                    <th >Action</th>
                </tr>
                
                <?php
                    $gender=array(0 => "Nữ",1=> "Nam");
                    $khoa=array(""=>"", "MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                    for($i=0; $i<count($data);$i++){
                        $dt = new DateTime($data[$i]['birthday']);
                        $date = $dt->format('d/m/Y');
                        echo '
                        <tr>
                            <td>'.$data[$i]['id'].'</td>
                            <td>'.$data[$i]['name'].'</td>
                            <td>'.$khoa[$data[$i]['faculty']].'</td>                            
                            <td>'.$gender[$data[$i]['gender']].'</td>
                            
                            <td>'.$date.'</td>
                            <td>'.$data[$i]['address'].'</td>
                            <td><input  type="submit" value="Xóa" name ="deletebtn"></td>
                            <td><input  type="submit" value="Sửa" name ="editbtn"></td>
                        ';
                    }
                ?>


            </table>
        </div>
        
    </form>
     <script>
        function myFunction(){
            document.getElementById('khoa').value='';
            document.getElementById('keyword').value='';
        }
    </script>
</body>
</html>