<?php
        require("grade.php");
        $answer0 = isset($_POST['answer0']) ? $_POST['answer0'] : '';
        $answer1 = isset($_POST['answer1']) ? $_POST['answer1'] : '';
        $answer2 = isset($_POST['answer2']) ? $_POST['answer2'] : '';
        $answer3 = isset($_POST['answer3']) ? $_POST['answer3'] : '';
        $answer4 = isset($_POST['answer4']) ? $_POST['answer4'] : '';
        setcookie("answer0", $answer0, time()+3600);
        setcookie("answer1", $answer1, time()+3600);
        setcookie("answer2", $answer2, time()+3600);
        setcookie("answer3", $answer3, time()+3600);
        setcookie("answer4", $answer4, time()+3600);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel = "stylesheet" href = "style.css">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>Trắc nghiệm</title>
</head>
<body>
    <?php
        
        if(($_SERVER['REQUEST_METHOD']=='POST') && isset($_POST['submit'])){
            header("Location: page2.php");
        }
    ?>
    <form class = "container" action="" method="post">
        <div class = "box_questions">
            <?php
           
            for($i=0;$i<count($questions)/2; $i++){
                echo'<div class="question">
                            '.($i+1).'. '.$questions[$i].'
                        </div>';
            
                foreach($results[$i] as $key=>$value){
                    echo'<div class="answers">
                                <input class="radio_answer" type="radio" name="answer'.$i.'" value="'.$key.'"> '.$value.'
                            </div>';
                };
        };  
            ?>
            <div class="btn">
                <button class="noselect" name="submit" type="submit">Next</button>

            </div>
            
        </div>
        
    </form>
</body>
</html>