<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['addbtn'])) {
        header("Location: signup.php");
        exit();
    }
?>
    <form class="container" method="post">
        <div class="search">
            <div class="search_khoa">
                <label >Khoa</label>
                <select name="khoa" id="">
                    <?php
                        $khoa=array(""=>"", "MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                        foreach($khoa as $key=>$value){
                            echo '<option value='.$key.'>'.$value.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="search_keyword_area">
                <label >Từ khóa</label>
                <input type="text" name="keyword">
            </div>
            <div class="search_btn">
                <input  type='submit' name ="submit" value = "Tìm kiếm"></input>
            </div>
        </div>
        <div class="show">Số sinh viên tìm thấy: XXX</div>
        <div class="add_btn">
            <input type="submit" value="Thêm" name ="addbtn">
        </div>
        <div class="show_edit_info" style="width = 100%;">
            <table>
                <tr>
                    <th width="7%">No</th>
                    <th width="30%">Tên sinh viên</th>
                    <th width="50%">Khoa</th>
                    <th >Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td><input class ='edit' type="submit" value="Xóa" name ="deletebtn"></td>
                    <td><input class ='edit' type="submit" value="Sửa" name ="editbtn"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td><input class ='edit' type="submit" value="Xóa" name ="deletebtn"></td>
                    <td><input class ='edit' type="submit" value="Sửa" name ="editbtn"></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td><input class ='edit' type="submit" value="Xóa" name ="deletebtn"></td>
                    <td><input class ='edit' type="submit" value="Sửa" name ="editbtn"></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td><input class ='edit' type="submit" value="Xóa" name ="deletebtn"></td>
                    <td><input class ='edit' type="submit" value="Sửa" name ="editbtn"></td>
                </tr>
            </table>
        </div>
        
    </form>
</body>
</html>