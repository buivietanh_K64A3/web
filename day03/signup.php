<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="box">  
            
                <form class="form" action="" method="post">
                    <div class="inputBox1">
                        <span>
                            Họ và tên
                        </span>
                        <input class="input1" type="text" required="required" name="name">
                    </div>

                    <div class="inputBox2">
                        <span class="gender">Giới tính</span>
                        <?php
                        $gender=array(0 => "Nam",1 => "Nữ");
                            for($i = 0; $i < count($gender); $i++){
                               echo '<input class="input2" type="radio" name="gender">
                                        <span class="">'.$gender[$i].'</span>';
                            }
                        ?>
                        
                    </div>

                    <div class="selectbox">
                        <span class="pk">Phân khoa</span>
                        <select class="input3">
                        <?php
                            $khoa = array("", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học dữ liệu");  
                            foreach ($khoa as $value) {
                                echo '<option value="'.$value.'">'.$value.'</option>';
                            }   
                        ?>
                        </select>
                        
                    </div>

                    <div >
                        <button class="boxbt" type ="submit"> Đăng ký</button>
                    </div>
                </form>
    </div>
</body>
</html>