<?php
// start session
session_start();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php
    $khoa = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học dữ liệu"); 
    $gender=array(0 => "Nam",1=> "Nữ");
?>
    <div class="box_confirm">  
            <form class="form_confirm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="inputBox1_confirm">
                                <span> Họ và tên</span> 
                                <p><?php echo $_SESSION['name'];?></p>
                                
                            </div>

                            <div class="inputBox2_confirm">
                                <span class="gender_confirm">Giới tính</span>
                                <span><?php echo $gender[$_SESSION['gender']];?></span>
                            </div>

                            <div class="selectbox_confirm">
                                <span class="pk_confirm">Phân khoa</span>
                                <span><?php echo $khoa[$_SESSION['khoa']];?></span>
                                
                            </div>

                            <div class="inputBox4_confirm">
                                <span> Ngày sinh </span>
                                <p><?php echo $_SESSION['birthday'];?></p>
                            </div>

                            <div class="inputBox5_confirm">
                                <span> Địa chỉ </span>
                                <p><?php echo $_SESSION['address'];?></p>
                            </div>

                            <div class="inputBox6_img_confirm">
                                <span>Hình ảnh</span>
                                <img src="<?php error_reporting(E_ALL ^ E_WARNING);echo $_SESSION['image']; ?>" class="image_confirm">
                               
                            </div>
                            <div class="boxbt" >
                                <button class="bt" type ="submit"> Xác nhận</button>
                            </div>

                            
                        </div>
                    </form>
    </div>
    
    
    </div>

</body>
</html>