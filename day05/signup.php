<?php
// start session
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php

    // define variables and set to empty values
    $name = $birthday = $gender = $address =$khoa =$image="";
    $nameErr= $birthdayErr = $genderErr = $addressErr =$khoaErr=$imageErr= "";
    // check if variable is set or not
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $gender=isset($_POST['gender'])?$_POST['gender']:'';
    $khoa=isset($_POST['khoa'])?$_POST['khoa']:'';
    $birthday=isset($_POST['birthday'])?$_POST['birthday']:'';
    $address=isset($_POST['address'])?$_POST['address']:'';
    $image=isset($_POST['image'])?$_POST['image']:'';
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // validate name
        if (empty($name)) {
          $nameErr = "Hãy nhập tên";
        } else {
          $_SESSION ['name'] = $name;
        }
        //validate birthday
        if (empty($birthday)) {
          $birthdayErr = "Hãy nhập ngày sinh";
        } else {
          $_SESSION ['birthday'] = $birthday;
          if((strlen($birthday)<10)OR(strlen($birthday)>10)){
            $birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";
          }
          else{
            
            //The entered value is checked for proper Date format
            if((substr_count($birthday,"/"))<>2){
              $birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";
            }
            else{
              $pos=strpos($birthday,"/");
              $date=substr($birthday,0,($pos));
              
              if(($date<=0)OR($date>31)){$birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";}
              
              $month=substr($birthday,($pos+1),($pos));
              if(($month<=0)OR($month>12)){$birthdayErr = "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy";}
            }
          }
        }
        //validate khoa
        if (empty($khoa)) {
          $khoaErr = "Hãy nhập khoa";
        } else {
          $_SESSION ['khoa'] = $khoa;
        }
        //validate gender
        if (strlen($gender)==0) {
          $genderErr = "Hãy chọn giới tính";
        } else {
          $_SESSION ['gender'] = $gender;
        }

        $_SESSION ['address'] = $address;

        // upload image after validation
        if (!file_exists("upload")){
          mkdir("upload");
        }
        date_default_timezone_set ("Asia/Ho_Chi_Minh");
        $time = date("YmdHis");
        // split image path
        $imageNameSplit = explode('.',  $_FILES["image"]["name"]);
        // combine datetime with image path
        error_reporting(E_ALL ^ E_WARNING);
        $imageNewName = "upload/" . $imageNameSplit[0] . '_' . $time . '.' . $imageNameSplit[1];

        $target_dir = "upload/";
        $target_file = $target_dir . basename( $_FILES["image"]["name"]); 
        $is_uploaded=true;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if (file_exists($target_file)){
            $is_uploaded=false;
        }
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            $is_uploaded=false;
            $imageErr = "Your file is not an image";
        }
        if($_FILES["image"]["size"] > 1000000) {
            $is_uploaded=false;
        }
        if ($is_uploaded==false){
            $imageErr= "Error. Your file was not uploaded.";
        }
        else{
            
          if (move_uploaded_file($_FILES["image"]["tmp_name"], $imageNewName)) { 
              $_SESSION['image']=$imageNewName??'';
               
          } else {
              $imageErr= "Sorry, there was an error uploading your file.";
          }
        }
        //redirect to ../day05/signup.php
        if (($name != "") && ($birthday != "") && ($gender != "") && ($khoa != "") && ($is_uploaded!=false)) {
             
            header("Location: ./confirm.php");
            exit();
        }
        
    }

    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
?>
    <div class="box">  
        <div class="error">
            <?php
                echo $nameErr ;
                echo "<br>";
                echo $genderErr;
                echo "<br>";
                echo $khoaErr;
                echo "<br>";
                echo $birthdayErr;
                echo "<br>";
                echo $imageErr;
                ?>
        </div>    
                <form class="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="inputBox1">
                            <span> Họ và tên <i>*</i></span> 
                            <input class="input1" type="text"  name="name">
                            
                        </div>

                        <div class="inputBox2">
                            <span class="gender">Giới tính<i>*</i></span>
                            <?php
                                $gender=array(0 => "Nam",1=> "Nữ");
                                    for($i = 0; $i < count($gender); ++$i){
                                    echo '<input class="input2" type="radio" name="gender" value = '.$i.'>
                                                <span class="" style= "margin-right:20px;" >'.$gender[$i].'</span>';
                                    }
                            ?>
                            
                        </div>

                        <div class="selectbox">
                            <span class="pk">Phân khoa<i>*</i></span>
                            <select class="input3" name="khoa">
                            <?php
                                $khoa = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học dữ liệu"); 
                                foreach ($khoa as $key=>$value) {
                                    echo '<option  value = '.$key.'>'.$value.'</option>';
                                }   
                            ?>
                            </select>
                            
                        </div>

                        <div class="inputBox4">
                            <span> Ngày sinh <i>*</i></span>
                                <input class='input4' id="txtDate" type="text" class="form-control date-input" name="birthday" />
                        
                        </div>

                        <div class="inputBox5">
                            <span> Địa chỉ </span>
                            <input class="input5" type="text"  name="address">
                            
                        </div>
                        <div class="inputBox6_img">
                          <span>Hình ảnh</span>
                          <input type ="file" class="input6" name="image"> 
                        
                        </div>
                        <div class="boxbt" >
                            <input class="bt" type ="submit" name ="submit" value="Đăng ký" > 
                        </div>

                        
                    </div>
                </form>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#txtDate').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
                

            });
        });
    </script>

<?php
// remove all session variables
session_unset();

// destroy the session
session_destroy();
?>
</body>
</html>